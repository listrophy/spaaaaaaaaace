# SPAAAAAAAAACE

Not much here, but check out `hover.rb`. It's a PID controller for a Kerbal Space Program ship that has a [Telemachus](https://github.com/richardbunt/Telemachus) part on it.

# Requirements

* [Kerbal Space Program](http://store.steampowered.com/app/220200/)
* [Telemachus](https://github.com/richardbunt/Telemachus/wiki/Installation)
* A spaceship in KSP, such as:
  * Mk1 Cockpit
  * Telemachus (mounted to cockpit)
  * FL-T400 Fuel Tank
  * LV-T45 "Swivel" Liquid Fuel Engine, preferably thrust-limited to about 60%
  * (3x) TT18-A Launch Stability Enhancer
* Rubygems:
  * kuby
  * excon
* Ruby 2.1 (though earlier probably works too)

# Usage

1. Fire up Kerbal Space Program
1. Enter sandbox mode
1. Build a spaceship as specified above
1. Go to the launchpad
1. Wait a couple seconds for things to settle
1. Run `hover.rb`

# Copyright

See MIT-LICENSE.md
