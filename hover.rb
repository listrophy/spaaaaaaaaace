require 'excon'
require 'kuby'

3.downto(1) do |i|
  print "#{i}... "
  sleep 1
end
puts "launch!"

link = Kuby::Link.new
link.connect! or fail 'Could not connect'

link.throttle_full
link.toggle_sas

link.stage! and sleep 2

target = 150.0 # hover height
prev_err = error =
  integral = derivative = 0.0

prev_time = link.mission_time

loop do
  cur_time = link.mission_time
  dt = cur_time - prev_time
  error = target - link.altitude
  integral = integral + error * dt
  derivative = (error - prev_err)/dt

  kp, ki, kd = 0.1, 0.003, 0.08
  new_throttle = (kp*error + ki*integral + kd*derivative)

  new_throttle = 1.0 if new_throttle > 1.0
  new_throttle = 0.0 if new_throttle < 0.0

  link.set_throttle new_throttle

  prev_err, prev_time = error, cur_time
  sleep 0.2
end
