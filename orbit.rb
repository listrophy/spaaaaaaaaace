fail "This file probably won't work"

require 'rubygems'
require 'excon'
require 'kuby'
require 'pry'

module Kuby
  class Link
    module BoosterMethods
      def solidFuel
        api_set('r.resource', 'SolidFuel')
      end

      def acceleration_near_zero
        true
      end
    end

    module AttitudeMethods
      def set_yaw new_yaw
        api_set('v.setYaw', new_yaw)
      end
    end

    module OrbitMethods

      def relativeVelocity
        api_get('o.relativeVelocity')
      end

      def apoapsis
        api_get('o.ApA')
      end

      def time_to_apoapsis
        api_get('o.timeToAp')
      end

    end

    include AttitudeMethods
    include BoosterMethods
    include OrbitMethods
  end
end



link = Kuby::Link.new
link.connect! or fail 'Could not connect'

# Set to full throttle
link.throttle_full

# Activate SAS
link.toggle_sas

puts 'connected.'
print 'Launching in 1...'
sleep 1
puts ' LIFTOFF!'

# Initiate first stage
link.stage!

print 'Stage 1 firing.'
begin
  sleep 1
  print '.'
end until link.solidFuel < 1
puts ' finished'
puts

puts 'Firing stage 2'
link.stage!

sleep 2
puts 'Tilt to 45deg along 90 (east)'
puts 'Hit enter when done'
gets

print 'Waiting for appropriate apoapsis'
begin
  sleep 1
  print '.'
end until link.apoapsis > 80_000
puts

puts 'Apoapsis of 80,000 achieved. Cutting throttle for coast'
link.throttle_zero

puts 'Orient horizontal!'
print 'Coasting'
begin
  sleep 1
  print '.'
end until link.time_to_apoapsis < 20
puts

puts 'Firing orbital insertion'
link.throttle_full

# begin
#   sleep 1
# end until link.stage_2_empty
# 
# link.stage!

__END__
target = 0.0
previous = 0.0
error = 0.0
integral = 0.0
derivative = 0.0
time = link.mission_time

#link.toggle_sas

puts ' pitch | error   | integral | derivative |'
puts '=========================================='

loop do
  dt = link.mission_time - time
  time = link.mission_time
  error = target - link.height
  integral = integral + error * dt
  derivative = (error - previous)/dt

  kp, ki, kd = 0.5, 0.8, 2
  kp, ki, kd = 1.0/10, 1.0/15, 1.0/30

  new_throttle = (kp * error + ki * integral + kd * derivative)

  #link.set_yaw new_yaw
  link.set_throttle new_throttle

  previous = error

  puts "| #{'%3.2f' % link.pitch} | #{'%3.2f' % link.roll} | #{'%3.2f' % link.heading} |"

  sleep 0.2
end
